// eslint-disable-next-line no-undef
module.exports = {
  apps: [
    {
      name: "Admin Panel Template",
      script: "npm run preview",
      args: "",
      instances: 1,
      autorestart: true,
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
