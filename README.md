# Nagorik Admin Panel Template

## Installation

```bash
$ git clone
$ cd nagorik-admin-panel
$ npm install
$ npm run dev
```

## Features

- [x] Login
- [x] Dashboard
- [x] User Management
- [x] Role Management
- [x] Permission Management
- [x] Statistics
- [x] Settings
- [x] Components

## Screenshots

## License

[MIT licensed](LICENSE).
