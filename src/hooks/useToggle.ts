import { useCallback, useState } from "react";

const useToggle = (
  initialValue: boolean | (() => boolean)
): [boolean, (value?: boolean) => void] => {
  const [value, setValue] = useState(
    typeof initialValue === "function" || typeof initialValue === "boolean"
      ? initialValue
      : Boolean(initialValue)
  );

  const toggle = useCallback(
    (value?: boolean) =>
      setValue((v) => (typeof value === "boolean" ? value : !v)),
    []
  );

  return [value, toggle];
};

export default useToggle;
