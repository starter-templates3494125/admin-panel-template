import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import App from "@/App.tsx";
import "@/index.css";
import NotFountPage from "@/pages/404";

import("preline");

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <NotFountPage />,
    children: [],
  },
  {
    path: "/login",
    element: null,
    errorElement: <NotFountPage />,
  },
  {
    path: "/register",
    element: null,
    errorElement: <NotFountPage />,
  },
  {
    path: "/404",
    element: <NotFountPage />,
  },
  {
    path: "*",
    element: <NotFountPage />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} fallbackElement={<NotFountPage />} />
  </React.StrictMode>
);
