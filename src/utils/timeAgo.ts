const intervals = [
  { label: "year", seconds: 31536000 },
  { label: "month", seconds: 2592000 },
  { label: "day", seconds: 86400 },
  { label: "hour", seconds: 3600 },
  { label: "minute", seconds: 60 },
  { label: "second", seconds: 1 },
];

const formatter = new Intl.PluralRules("en", { type: "ordinal" });

export const formatTimeAgo = (timestamp: string): string => {
  const date = new Date(timestamp);
  const now = new Date();
  const diff = Math.abs(now.getTime() - date.getTime());

  // Get time in seconds
  const seconds = Math.floor(diff / 1000);

  // Find the largest interval that fits into the given time
  const interval = intervals.find((i) => seconds >= i.seconds);

  if (interval) {
    const count = Math.floor(seconds / interval.seconds);
    const unit =
      formatter.select(count) === "one" ? interval.label : `${interval.label}s`;
    return `${count} ${unit} ago`;
  }

  // If the timestamp is in the future, return "just now"
  return "just now";
};
