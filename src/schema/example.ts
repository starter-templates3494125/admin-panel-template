import { z } from "zod";

export const exampleSchema = z.object({
  name: z.string(),
  age: z.number(),
  isCool: z.boolean(),
  favoriteFoods: z.array(z.string()),
  favoriteNumbers: z.array(z.number()),
  favoriteColors: z.array(z.string()),
});

export type Example = z.infer<typeof exampleSchema>;
