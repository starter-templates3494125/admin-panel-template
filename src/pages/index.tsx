import { Link } from "react-router-dom";

import Layout from "@/layout";

const IndexPage = () => {
  return (
    <Layout>
      <h1>Home Page</h1>
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  );
};

export default IndexPage;
