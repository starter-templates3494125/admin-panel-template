import { create } from "zustand";

type CounterStore = {
  count: number;
  increment: () => void;
  decrement: () => void;
};

const useCounterStore = create<CounterStore>()((set) => ({
  count: 0,
  increment: () => set((state: CounterStore) => ({ count: state.count + 1 })),
  decrement: () =>
    set((state: CounterStore) =>
      state.count > 0 ? { count: state.count - 1 } : { count: 0 }
    ),
}));

export default useCounterStore;
