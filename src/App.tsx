import useCounterStore from "@/stores/counter";
import { Suspense, lazy } from "react";

import Spinner from "@/components/Spinner";

const Button = lazy(() => import("@/components/Button"));

const App = () => {
  const { count, increment, decrement } = useCounterStore();

  return (
    <main className="container mx-auto space-y-4 my-4">
      <h1 className="text-2xl font-semibold">Admin Template</h1>

      <div className="space-y-4">
        <p className="flex flex-col bg-white border shadow-sm rounded-xl p-4 md:p-5 dark:bg-gray-800 dark:border-gray-700 dark:shadow-slate-700/[.7] dark:text-gray-400">
          <span className="text-lg font-semibold">Counter</span>
          <span className="text-gray-500">Count: {count}</span>
        </p>
        <div className="flex gap-4">
          <Suspense fallback={<Spinner />}>
            <Button text="Increment 1" onClick={increment} />
            <Button text="Decrement 1" onClick={decrement} />
          </Suspense>
        </div>
      </div>
    </main>
  );
};

export default App;
