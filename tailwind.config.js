/** @type {import('tailwindcss').Config} */
export default {
  content: [
    // Preline
    "node_modules/preline/dist/*.js",

    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/container-queries"),
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),

    // Preline
    require("preline/plugin"),
  ],
};
